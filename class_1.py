class Patient:
    def __int__(self, name, surname, age, gender):
        self.name = name
        self.surname = surname
        self.age = age
        self.gender = gender

    def __repr__(self):
        return f'Name = {self.name}\nSurname = {self.surname}\nAge = {self.age}\nGender = {self.gender}'


class Doctor:
    def __init__(self, name, surname, schedule):
        self.name = name
        self.surname = surname
        self.schedule = schedule

    def __repr__(self):
        return f'Name = {self.name}\nSurname = {self.surname}\nSchedul = {self.schedule}'

    def register(self):
        Doctor.register('Andrey', {'13:30': patient_1})
        Doctor.regidter('Anna', {'15:48': patient_2})

patient_1 = Patient("Andrey", "Abrahamyan", 25, 'f')
patient_2 = Patient("Anna", "Minasyan", 16, 'm')
doc_1 = Doctor('Eduard', 'Xachatryan', {'13:30': patient_1})
doct_1 = Doctor('Eduard', 'Xachatryan', {'15:48': patient_2})
